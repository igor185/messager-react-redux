import {SEND_MESSAGE} from "../Send/actionTypes";
import {DELETE_MESSAGE, SET_LIKE, SET_MESSAGES} from "./actionTypes";
import findInArray from "../../helpers/findInArray.helper";
import {CHANGE_MESSAGE} from "../Modal/actionTypes";

const initState = {
    messages: [],
    user:{
        user: "Dave",
        avatar: "https://i.pravatar.cc/300?img=14"
    }
};

export default function(state = initState, action){
    switch (action.type) {
        case SET_MESSAGES:
            return{
                ...state,
                messages: action.payload.messages
            };
        case SEND_MESSAGE:
            return{
              ...state,
              messages:[...state.messages, action.payload.message]
            };
        case DELETE_MESSAGE:
            let messages = state.messages;
            let i = findInArray(messages, action.payload.id, 'id');
            messages.splice(i, 1);
            return {
                ...state,
                messages: [...messages]
            };
        case SET_LIKE:
            let messagesToLike = state.messages;
            let k = findInArray(messagesToLike, action.payload.id, 'id');
            let reactions = messagesToLike[k].reactions;
            if (reactions) {
                let j = findInArray(reactions, state.user.user, 'user');
                if (j === -1)
                    reactions = [...reactions, state.user];
                else
                    reactions.splice(j, 1);
            } else
                reactions = [state.user];
            messagesToLike[k].reactions = reactions;
            return {
                ...state,
                messages:[...messagesToLike]
            };
        case CHANGE_MESSAGE:
            const messages2 = state.messages;
            const index = findInArray(messages2, action.payload.id, 'id');
            if (index === -1)
                return state;
            messages2[index].message = action.payload.value;
            return {
                ...state,
                messages: [...messages2]
            };
        default:
            return state;
    }
}