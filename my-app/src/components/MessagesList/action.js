import {DELETE_MESSAGE, SET_LIKE, SET_MESSAGES} from "./actionTypes";

export const deleteMessage = (id) =>{
    return {
        type: DELETE_MESSAGE,
        payload:{
            id
        }
    }
};
export const setLike = (id) =>{
    return {
        type: SET_LIKE,
        payload:{
            id
        }
    }
};

export const setMessages = (messages) =>{
    return {
        type:SET_MESSAGES,
        payload:{
            messages
        }
    }
};