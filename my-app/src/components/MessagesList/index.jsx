import React from 'react';
import Message from '../Message/index';
import TimeLine from '../TimeLine/index';
import './style.css'

import {deleteMessage, setLike} from './action';
import {showModal} from "../Modal/action";
import {connect} from "react-redux";

class MessagesList extends React.Component {

    // componentDidUpdate() {
    //     const list = document.getElementById('messagesList');
    //     list.scrollTo(0, list.scrollHeight);
    // }

    render() {
        let now = new Date();
        let messages = [...this.props.messages];
        let messages_Time = [];
        let tempDate = new Date();
        for (let i = 0; i < messages.length; i++) {
            let j = new Date(messages[i].created_at);
            if (tempDate && tempDate.getDay() !== j.getDay()) {
                if (j.getDay() === now.getDay())
                    messages_Time.push(<TimeLine time={'Today'} key={j.toLocaleString()}/>);
                else if (j.getDay() + 1 === now.getDay())
                    messages_Time.push(<TimeLine time={'Yesterday'} key={j.toLocaleString()}/>);
                else
                    messages_Time.push(<TimeLine time={j.toLocaleDateString().split(',')[0]}
                                                 key={j.toLocaleString()}/>);
            }
            tempDate = j;
            let elem = messages[i];
            messages_Time.push(<Message key={elem.id} setLike={this.props.setLike}
                                        deleteMessage={() => this.props.deleteMessage(elem.id)}
                                        changeMessageText={this.props.changeMessageText} message={elem}
                                        user={this.props.user}
            showModal={() => this.props.showModal(elem.id)}/>)
        }

        return (
            <div className={"border messagesList"} id={"messagesList"} style={{height: "calc(100vh - 80px)"}}>
                {messages_Time}
            </div>
        )
    }
}
const mapStateToProps = (state, ownProps) => {
    return {
        ...ownProps,
        user:state.chat.user
    };
};

const mapDispatchToProps = {
    deleteMessage,
    setLike,
    showModal
};

export default connect(mapStateToProps, mapDispatchToProps)(MessagesList);