import {SEND_MESSAGE} from "./actionTypes";

export const sendMessage = (message) =>{
    return{
        type:SEND_MESSAGE,
        payload:{
            message
        }
    }
};