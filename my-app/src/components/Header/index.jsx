import React from 'react';
import './index.css'
export default class Header extends React.Component {

    render() {
        const messages = [...this.props.messages];
        const amountUsers = this.countUniqUsers(messages);
        const lastMessTime = new Date(messages[messages.length-1].created_at);
        const amountMessages = messages.length;

        return (
            <header className={'header border'}>
                <div>
                    {this.props.name}
                </div>
                <div>
                    {amountUsers} participants
                </div>
                <div>
                    {amountMessages} messages
                </div>
                <div>
                    last message {lastMessTime.toLocaleString()}
                </div>
            </header>
        );
    }

    countUniqUsers(messages){
        const users = new Set();
        messages.forEach(elem => users.add(elem.user));
        return users.size;
    }
}