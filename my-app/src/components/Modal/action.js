import {CHANGE_MESSAGE, HIDE_MODAL, SHOW_MODAL} from "./actionTypes";

export const showModal = (messageId) =>{
    return{
        type: SHOW_MODAL,
        payload:{
            messageId
        }
    }
};

export const hideModal = () =>{
    return{
        type: HIDE_MODAL
    }
};

export const changeMessage = (id, value)=>{
    return{
        type: CHANGE_MESSAGE,
        payload:{
            id, value
        }
    }
};