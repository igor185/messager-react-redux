import {HIDE_MODAL, SHOW_MODAL} from "./actionTypes";

const initState = {
    messageId:'',
    isShown: false
};

export default function(state = initState, action){
    switch (action.type) {
        case SHOW_MODAL:
            return{
                ...state,
                isShown: true,
                messageId: action.payload.messageId
            };
        case HIDE_MODAL:
            return{
                ...state,
                isShown: false,
                messageId: ''
            };
        default:
            return state;
    }
}