import React from 'react';
import Header from '../Header/index';
import MessagesList from '../MessagesList/index';
import Send from '../Send';

class Chat extends React.Component {


    render() {
        return (
            <div className={'container'}>
                <Header name={this.props.chatName} messages={this.props.messages}/>
                <MessagesList messages={this.props.messages} changeMessageText={this.props.changeMessageText}/>
                <Send/>
            </div>
        )
    }
}


export default Chat;