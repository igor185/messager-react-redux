import React from 'react';

export default class TextArea extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            value: props.value
        }
    }

    render(){
        return(
            <textarea name="" id="" cols="50" rows="5" defaultValue={this.state.value} onChange={(e)=> this.props.change(e.target.value)}/>
        )
    }
    // change(e){
    //     console.log(e.target.value);
    //     this.setState({value: e.target.value});
    // }
}