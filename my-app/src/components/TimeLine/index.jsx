import React from 'react'
import './style.css'
export default class TimeLine extends React.Component{


    render(){

        return(
            <div className={'time-line-wrp'}>
                <span className={'time-line'}>{this.props.time}</span>
            </div>
        )
    }
}