import React from 'react'
import './style.css';


export default class Icon extends React.Component{

    render() {

        return (
            <img src={this.props.src} className={'icon'} alt="icon"
                 onClick={() => this.props.callback ? this.props.callback() : this.props}/>
        )
    }
}