const findInArray = (arr, value, key) => {
    for (let i = 0; i < arr.length; i++)
        if (arr[i][key] === value)
            return i;
    return -1;
};
export default findInArray;