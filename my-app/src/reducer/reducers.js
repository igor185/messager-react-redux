import {combineReducers} from "redux";
import chat from '../components/MessagesList/reducer';
import modal from '../components/Modal/reducer';

export const rootReducer = combineReducers({chat, modal});