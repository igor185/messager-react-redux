import React from 'react';
import Chat from './components/Chat/index';
import Spinner from './components/Spinner';
import Modal from './components/Modal';

import {connect} from 'react-redux';
import {setMessages} from "./components/MessagesList/action";


const MESSAGES_URL = 'https://api.myjson.com/bins/1hiqin';

const chatName = "myChat";

class App extends React.Component {
    constructor(props) {
        super(props);
        this.getMessages();
    }

    getMessages() {
        if (!this.props.messages || this.props.messages.length === 0) {
            fetch(MESSAGES_URL)
                .then(data => data.json())
                .then(data => this.props.setMessages(data))
                .catch(e => document.write(e.message));
        }
    }

    render() {
        return (
            <div>
                {this.props.messages && this.props.messages.length !== 0 ?
                    (<Chat messages={this.props.messages} chatName={chatName}
                           changeMessageText={(id, text) => this.changeMessageText(id, text)}/>)
                    :
                    (<Spinner/>)}
                    <Modal/>
            </div>
        )
    }

    changeMessageText(id, text) {
        this.setState((state) => {
            const messages = state.messages;
            const index = findInArray(messages, id, 'id');
            if (index === -1)
                return;
            messages[index].message = text;
            return {messages};
        })
    }
}

const mapStateToProps = (state) => {
    return {
        messages: state.chat.messages,
        user: state.chat.user
    };
};

const mapDispatchToProps = {
    setMessages
};

export default connect(mapStateToProps, mapDispatchToProps)(App);

function findInArray(arr, value, key) {
    for (let i = 0; i < arr.length; i++)
        if (arr[i][key] === value)
            return i;
    return -1;
}
